import * as React from 'react';

// Component for the form used to enqueue new aircrafts 

class NewACForm extends React.Component {
    
    constructor() {
        super();
        this.state = { 
            id: "",
            type: "",
            size: ""
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    
    handleSubmit(event) {

        //check for bad input
        if (!(/[^a-zA-Z0-9]/.test(this.state.id)) && 
            this.state.id !== '' &&
            this.state.type !== '' &&
            this.state.size !== '') {
            
            //API request to enqueue
            fetch('http://localhost:8080/enqueue-AC/' + 
                   this.state.id + '-' +
                   this.state.type + '-' +
                   this.state.size);
        } else {
            alert("Invalid input");
            event.preventDefault();
        }
    }
 
    render() { 
        return (
            <div>
            <h2>New Aircraft:</h2>
            <form>
                <label>
                Aircraft ID:
                    <input
                    name = "id"
                    value = {this.state.id}
                    onChange = {this.handleChange}
                    />
                </label>
                <br/>
                <label>
                Type:
                    <select name = "type" value = {this.state.value} onChange = {this.handleChange}>
                        <option value=""></option>
                        <option value="emergency">Emergency</option>
                        <option value="vip">VIP</option>
                        <option value="passenger">Passenger</option>
                        <option value="cargo">Cargo</option>
                    </select>
                </label>
                <br/>
                <label>
                Size:
                    <select name = "size" value = {this.state.value} onChange = {this.handleChange}>
                        <option value=""></option>
                        <option value = "0">Small</option>
                        <option value = "1">Large</option>
                    </select>
                </label>
                <br/><br/>
                <button onClick = {this.handleSubmit}>Queue Aircraft</button>
            </form>
            </div>
        );
    }
}
 
export default NewACForm;