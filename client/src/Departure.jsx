import * as React from 'react';



class NewACForm extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {lastDeparted: props.lastDeparted};
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleSubmit(event) {
        fetch('http://localhost:8080/dequeue-AC/')
        .then(response => response.json())
        .then(data => {this.setState({lastDeparted: data})});  
    }
 
    render() { 
        if (this.state.lastDeparted === null) {
            return(<button onClick = {this.handleSubmit}>Dequeue Aircraft</button>);
        }

        return (
            <div>
                <br/>
                <button onClick = {this.handleSubmit}>Dequeue Aircraft</button>
                <h2>Departure:</h2>
                
                <table align="center"><tbody>
                    <tr key={this.state.lastDeparted.id}>
                        <td>{this.state.lastDeparted.id}</td>
                        <td>{this.state.lastDeparted.priority}</td>
                        <td>{this.state.lastDeparted.size}</td>
                    </tr>
                </tbody></table>
            </div>
        );
    }
}
 
export default NewACForm;