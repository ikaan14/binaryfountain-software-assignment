import React from 'react';
import './App.css';
import NewACForm from './NewACForm'
import ACQueue from './ACQueue'

function App() {
  return (
    <div className="App">
      <br/>
      <NewACForm/>
      <br/>
      <ACQueue/>
    </div>
  );
}

export default App;
