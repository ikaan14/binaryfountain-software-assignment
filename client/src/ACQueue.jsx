import * as React from 'react';
import './ACQueue.css';

var lastUpdate;

// Component for displaying the contents of the queue and handling departures (dequeuing)

class ACQueue extends React.Component {
    
    constructor() {
        super();
        this.state = { 
            ACQueueList: [],
            lastDeparted: null,
            isLoading: false
        };
        this.handleDequeue = this.handleDequeue.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
    }
    
    // on component mount, controller is initialized 
    componentDidMount() {
        this.setState({isLoading: true});
        fetch('http://localhost:8080/init-controller')
        .then(response => {
            if (response === false) {
                console.log("Error Initializing Controller");
            } else {
                fetch('http://localhost:8080/list-queue')
                .then(response => response.json())
                .then(data => {
                    this.setState({ACQueueList: data, isLoading: false});
                    lastUpdate = Date.now();
                });
            }
        });
    }

    handleDequeue = (event) => {
        // Check if UI is up to date, if not prompt user to refresh
        fetch ('http://localhost:8080/check-last-update', {method : 'POST', body: lastUpdate.toString()})
        .then(response => response.json())
        .then(isUpdated => {
            if(isUpdated && this.state.ACQueueList.length > 0) {
                fetch('http://localhost:8080/dequeue-AC')
                .then(response => response ? response.json() : null)
                .then(departed => {
                    fetch('http://localhost:8080/list-queue')
                    .then(response => response.json())
                    .then(queue => {
                        this.setState({ACQueueList: queue, lastDeparted: departed, isLoading: false});
                        lastUpdate = Date.now();
                    });
                })
                .catch(err => console.log(err));
            } else {
                alert("Queue not updated, refresh queue before trying to dequeue again.")
            }
        });
    }

    handleRefresh(event) {
        fetch('http://localhost:8080/list-queue')
            .then(response => response.json())
            .then(queue => {
                this.setState({ACQueueList: queue, lastDeparted: null, isLoading: false});
                lastUpdate = Date.now();
            }) 
            .catch(err => console.log(err));
    }

    // Takes an Aircraft JSON and displays it as a table listing  
    generateListing(AC) {
        
        var sizeStr = AC.size === 0 ? 'Small' : 'Large';
        var typeStr;

        switch(AC.priority) {
            case 3:
                typeStr = 'Emergency';
                break;
            case 2:
                typeStr = 'VIP';
                break;
            case 1:
                typeStr = 'Passenger';
                break;
            case 0:
                typeStr = 'Cargo'
                break;
            default:
        }

        return (
            <tr key={AC.id}>
                <td>{AC.id}</td>
                <td>{typeStr}</td>
                <td>{sizeStr}</td>
            </tr>
        );
    }
 
    render() { 

        const {ACQueueList, isLoading} = this.state;

        if (isLoading) {
            return <p>Initializing...</p>;
        } 

        let dequeueButton = this.state.ACQueueList.length > 0 ? 
            <button onClick = {this.handleDequeue}>Dequeue Aircraft</button> : null;

        let departureDiv = 
            (this.state.lastDeparted === null) ? 
            (
            <div>
                <br/>
                {dequeueButton}
            </div>
            ) : (
            <div>
                <br/>
                {dequeueButton}
                <h2>Departure:</h2>
                
                <table align="center"><tbody>
                    {this.generateListing(this.state.lastDeparted)}
                </tbody></table>
            </div>
            );

        return (
            <div>
                <div>
                    <h2>Queue:</h2>
                    <table align="center">
                    <tbody>
                        <tr>
                            <th>ID</th>
                            <th>Type</th>
                            <th>Size</th>
                        </tr>
                        {ACQueueList.map((AC) =>
                            this.generateListing(AC)
                        )}
                    </tbody>
                    </table>
                    <br/>
                    <button onClick = {this.handleRefresh}>Refresh</button>
                </div>
                {departureDiv}
            </div>
        );
    }
}
 
export default ACQueue;