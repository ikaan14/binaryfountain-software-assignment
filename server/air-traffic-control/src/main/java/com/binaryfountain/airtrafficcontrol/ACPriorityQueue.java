package com.binaryfountain.airtrafficcontrol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

/* 
 * The ACPriorityQueue class represents the data structure used to maintain the 
 * state of the air-traffic controller. This priority queue implementation leverages 
 * java's linked-list class to obtain constant time dequeuing and linear time enqueuing,
 * while allowing the contents to be listed in the correct order in linear time as 
 * well (not possible with heap-based implementation).
 * 
 * NOTE: While multi-controller usage is supported by the ACQueueController class,
 * Concurrent operation calls are not allowed to ensure that simultaneous attempts 
 * to dequeue the same aircraft from multiple controllers will not result in multiple 
 * aircrafts being dequeued.  
 */
public class ACPriorityQueue implements Serializable{

	// Default UID
	private static final long serialVersionUID = 1L;
	
	private LinkedList<Aircraft> ACList;
	private static boolean modificationFlag = false;
	
	public ACPriorityQueue() {
		ACList = new LinkedList<Aircraft>();
	}

	public void enqueue(Aircraft AC) {
		modificationFlag = true;
		int i = ACList.size();
		while(i > 0 && AC.compareTo(ACList.get(i - 1)) > 0) {
			i--;
		}
		ACList.add(i, AC);
		modificationFlag = false;
	}
	
	public Aircraft dequeue() throws Exception {
		if (modificationFlag == true) {
			throw new Exception("Concurrent modification");
		}
		modificationFlag = true;
		Aircraft dequeued = ACList.poll();
		modificationFlag = false;
		return dequeued;
	}
	
	public boolean containsID(String id) {
		for(Aircraft AC : ACList) {
			if (AC.getId().equals(id))
				return true;
		}
		return false;
	}
	
	public ArrayList<Aircraft> toList() {
		ArrayList<Aircraft> contents = new ArrayList<Aircraft>();
		ACList.forEach((AC) -> contents.add(AC));
		return contents;
	}
	
	public int getSize() {
		return ACList.size();
	}
}
