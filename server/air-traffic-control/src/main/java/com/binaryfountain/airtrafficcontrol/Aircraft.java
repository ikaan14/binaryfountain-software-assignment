package com.binaryfountain.airtrafficcontrol;

import java.io.Serializable;

/*
 * The Aircraft class defines the data structure used to hold the information for 
 * aircrafts being operated on by the air-traffic controller. The different types 
 * of aircrafts are represented by subclasses of this class (see EmergencyAC, VipAC,
 * PassengerAC, and CargoAC classes).
 * 
 * NOTE: While the logic necessary for this assignment may be easy to implement
 * using one class, having a class for each type seemed more intuitive and allows 
 * for unique operations for certain types of aircrafts, were they ever to be implemented. 
 */

public class Aircraft implements Serializable {
	
	// Default UID
	private static final long serialVersionUID = 1L;
	private String id;
	private int priority;
	private int size;
	
	
	public Aircraft(String id, int priority, int size){
		this.id = id;
		this.priority = priority;
		this.size = size <= 0 ? 0 : 1;
	}
	
	public String getId() {
		return id;
	}
	
	public int getPriority(){
		return priority;
	}
	
	public int getSize(){
		return size;
	}
	
	public int compareTo(Aircraft other) {
		if (priority < other.priority)
			return -1;

		else if (priority > other.priority)
			return 1;

		else {
			if (size < other.size)
				return -1;
			else if (size > other.size)
				return 1;
		}
		return 0;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this.id.equals(((Aircraft)other).id) &&
			this.compareTo((Aircraft)other) == 0) {
			return true;
		}
		return false;	
	}
	
	public String toString() {
		return ( 
			this.getId() + "-" + 
			this.getClass().getName() + "-" + 
			this.getPriority()
			);
	}
}
