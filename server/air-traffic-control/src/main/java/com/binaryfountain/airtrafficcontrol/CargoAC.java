package com.binaryfountain.airtrafficcontrol;

public class CargoAC extends Aircraft {

	private static final long serialVersionUID = 1L;

	public CargoAC(String id, int size) {
		super(id, 0, size);
	}
}
