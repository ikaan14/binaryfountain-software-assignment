package com.binaryfountain.airtrafficcontrol;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/*
 * The ACQueueController class provides the functionality for operating the air-taffic
 * controller using a priority queue, as well as end-points for utilizing the controller 
 * through REST API requests. All instances of this class share the same 
 * queue, thus multi-controller usage is supported. 
 */

@RestController
public class ACQueueController {
	
	private static ACPriorityQueue controllerQueue;
	private static final String filePath = System.getProperty("catalina.home") + "/ACQueue";
	
	// For testing with local storage instead of embedded tomcat server, use this path with the correct PROJECT_PATH
	//	private static final String filePath = 
	//			"<PROJECT_PATH>/Server/air-traffic-control/src/test/java/com/binaryfountain/airtrafficcontrol/ACQueue";
	
	private static Long lastUpdateMs;
	
	/*
	 *  initialize controller by loading controllerQueue object from tomcat server,
	 *  or by creating a new queue if no file already exists.
	 */
	@GetMapping("/init-controller")
	@CrossOrigin(origins = "http://localhost:3000")
	public boolean initController() {
		File f = new File(filePath);
		if (f.exists()) {
			FileInputStream fileIn;
			try {
				fileIn = new FileInputStream(f);
				ObjectInputStream objectIn = new ObjectInputStream(fileIn);
				controllerQueue = (ACPriorityQueue)objectIn.readObject();
				objectIn.close();
				System.out.println("ACQueue read from file");
				lastUpdateMs = System.currentTimeMillis();
				return true;
			} catch (Exception e) {
				System.out.println("Error reading ACQueue from TomCat Server:" + "/n" + e);
				// No recovery method, so failure to load results in new queue
				controllerQueue = new ACPriorityQueue();
				return false;
			}
		}
		else {
			System.out.println("No existing ACQueue file");
			controllerQueue = new ACPriorityQueue();
			return true; 
		}
	}
		
	/*
	 * Enqueue a new aircraft based on the type and size of the aircraft, and save
	 * queue to file.
	 */
	@GetMapping("/enqueue-AC/{id}-{type}-{size}")
	@CrossOrigin(origins = "http://localhost:3000")
	public boolean enqueueAC(@PathVariable("id") String id,
							 @PathVariable("type") String type, 
						     @PathVariable("size") int size) {
		
		if (controllerQueue == null) 
			throw new NullPointerException("Attempting to operate on non-initialized controller");
		
		// Check for special characters or duplicate IDs
		Pattern p = Pattern.compile("[^a-zA-Z0-9]|^$");
		
		if(p.matcher(id).find() || controllerQueue.containsID(id))
			return false;
		
		switch(type) { 
			case "emergency": 
				controllerQueue.enqueue(new EmergencyAC(id, size));
				break;
			case "vip": 
				controllerQueue.enqueue(new VipAC(id, size));
				break;
			case "passenger": 
				controllerQueue.enqueue(new PassengerAC(id, size));
				break;
			case "cargo": 
				controllerQueue.enqueue(new CargoAC(id, size));
				break;
			default:
				//error 
				return false;
		}
		this.saveQueue();
		return true;
	}
	
	/*
	 * Dequeue an aircraft from controllerQueue, returns the dequeued aircraft or
	 * null if either the queue is empty or concurrent modification is detected. 
	 * Saves queue to file on successful dequeue.
	 */
	@GetMapping("/dequeue-AC")
	@CrossOrigin(origins = "http://localhost:3000")
	public Aircraft dequeueAC(){
		
		if (controllerQueue == null)
			throw new NullPointerException("Attempting to operate on non-initialized controller");
	
		if (controllerQueue.getSize() == 0)
			return null;
			
		Aircraft next = null;
		try {
			next = controllerQueue.dequeue();
			this.saveQueue();
		} catch (Exception e) {
			System.out.println("Error: concurrent modification");
		}
		return next;
	}
	
	/*
	 *  returns an ArrayList representation of the controllerQueue in the correct order
	 */
	@GetMapping("/list-queue")
	@CrossOrigin(origins = "http://localhost:3000")
	public ArrayList<Aircraft> listQueue() {
		if (controllerQueue == null)
			throw new NullPointerException("Attempting to operate on non-initialized controller");
		
		return controllerQueue.toList();
	}
	
	/*
	 *  Takes a timestamp string (in milliseconds) and returns true if the timestamp 
	 *  is after the last modification time of the controllerQueue, o.w returns false
	 */
	
	@PostMapping("/check-last-update")
	@CrossOrigin(origins = "http://localhost:3000")
	public boolean checkLastUpdate(@RequestBody String clientUpdateMs) {
		if (controllerQueue == null)
			throw new NullPointerException("Attempting to operate on non-initialized controller");
	
		if (Long.parseLong(clientUpdateMs) < lastUpdateMs)
			return false;
		else
			return true;
	}
	
	/*
	 * Helper: Saves queue to the embedded tomcat server
	 */
	private boolean saveQueue() {
		File f = new File(filePath);
		try {
			FileOutputStream fileOut = new FileOutputStream(f);
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(controllerQueue);
			objectOut.close();
			lastUpdateMs = System.currentTimeMillis();
		} catch (Exception e) {
			System.out.println("Error saving ACQueue to tomcat server");
			return false;
		}
		return true;
	}
	
	/*
	 * for testing
	 */
	public void clearQueue(){
		controllerQueue = new ACPriorityQueue();
		saveQueue();
	}
}
