package com.binaryfountain.airtrafficcontrol;

public class PassengerAC extends Aircraft {

	
	private static final long serialVersionUID = 1L;

	public PassengerAC(String id, int size) {
		super(id, 1, size);
	}
}
