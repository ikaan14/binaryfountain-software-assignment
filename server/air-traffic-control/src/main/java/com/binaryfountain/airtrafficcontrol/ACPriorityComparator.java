package com.binaryfountain.airtrafficcontrol;

import java.util.Comparator;

public class ACPriorityComparator implements Comparator<Aircraft> {

	@Override
	public int compare(Aircraft o1, Aircraft o2) {
		// higher priority comes first
		return -o1.compareTo(o2);
	}
}
