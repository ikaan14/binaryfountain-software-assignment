package com.binaryfountain.airtrafficcontrol;

public class EmergencyAC extends Aircraft {
	
	private static final long serialVersionUID = 1L;

	public EmergencyAC(String id, int size) {
		super(id, 3, size);
	}
}
