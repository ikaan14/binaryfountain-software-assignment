package com.binaryfountain.airtrafficcontrol;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Test;

public class testController {

	/*
	 * Test passing bad values to functions 
	 */
	@Test
	public void testBadValues() {
		ACQueueController ctrl = new ACQueueController();
		ctrl.initController();
		ctrl.clearQueue();
		assertFalse(ctrl.enqueueAC(":foo", "Cargo", 0));
		assertFalse(ctrl.enqueueAC("", "Cargo", 0));
		assertFalse(ctrl.enqueueAC("A123", "sdfsdf", 0));
		// nothing should be on the queue
		assertTrue(ctrl.listQueue().size() == 0);
		assertEquals(null, ctrl.dequeueAC());
	}
	
	/*
	 * Test large number of random queues and dequeues
	 * NOTE: may need to change "filepath" in ACQueueController to a 
	 * local file path
	 */
	@Test
	public void stressTestQueue() {
		ACQueueController ctrl = new ACQueueController();
		ctrl.initController();
		ctrl.clearQueue();
		Random rnd = new Random(100000);
		ArrayList<Aircraft> expected = new ArrayList<Aircraft>();
		
		for(int i = 0 ; i < 1000 ; i++) {
			
			if (i%5 == 4) {
				Aircraft depart = ctrl.dequeueAC();
				expected.remove(depart);
			}
			String type = "";
			Aircraft AC = null;
			
			String id = "" + i;
			int priority = rnd.nextInt(4);
			int size = rnd.nextInt(2);
			
			switch(priority) {
	            case 3:
	                type = "emergency";
	                AC = new EmergencyAC(id, size);
	                break;
	            case 2:
	                type = "vip";
	                AC = new VipAC(id, size);
	                break;
	            case 1:
	                type = "passenger";
	                AC = new PassengerAC(id, size);
	                break;
	            case 0:
	                type = "cargo";
	                AC = new CargoAC(id, size);
	                break;
	            default:
	        }
			ctrl.enqueueAC(("" + i), type, size);
			expected.add(AC);
		}
		expected.sort(new ACPriorityComparator());
		ArrayList<Aircraft> actual = ctrl.listQueue();
		for(int i = 0 ; i < expected.size() ; i++) {
			assertTrue(expected.get(i).compareTo(actual.get(i)) == 0);
		}
	}
	/*
	 * make sure calls to functions throw exceptions if controller isn't initialized
	 * (can only be run independently)
	 */
//	@Test
//	public void testNoInit() {
//		String exceptionStr = "Attempting to operate on non-initialized controller";
//		ACQueueController ctrl = new ACQueueController();
//		
//		try{
//			ctrl.listQueue();
//			fail();
//		}catch(Exception e){assertEquals(exceptionStr,e.getMessage());}
//		
//		try{
//			ctrl.enqueueAC("foo", "Emergency", 0);
//			fail();
//		}catch(Exception e){assertEquals(exceptionStr,e.getMessage());}
//			
//		try{
//			ctrl.dequeueAC();
//			fail();
//		}catch(Exception e){assertEquals(exceptionStr,e.getMessage());}
//		
//		try{
//			ctrl.checkLastUpdate("1234567890");
//			fail();
//		}catch(Exception e){assertEquals(exceptionStr,e.getMessage());}
//	}
}
