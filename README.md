BINARY FOUNTAIN - SIMPLE AIR TRAFFIC CONTROLLER ASSIGNMENT

by Kaan Ibici

This project consists of a java application for the server and a react application for the UI. 

NOTE: This project implements persistent storage, however it is currently set to use an embedded tomcat server that does not persist across application reboots. To test the persistent storage, modify the ACQueueController class (located in the com.binaryfountain.airtrafficcontrol package) so that the file path points to a file on the local system (if the specified file doesn't exist, it will be created) then run the following in the server/air-traffic-control directory: 

mvn clean package


See individual classes and components for more details/explanation on implementation.


HOW TO RUN THE PROJECT:

1) From the server/air-traffic-control directory run:

java -jar target/air-traffic-control-0.0.1-SNAPSHOT.jar

this will also expose the following REST API mappings on localhost:8080 :

	"/init-controller" to initialize a controller with a queue loaded from file

	"/enqueue-AC/{id}-{type}-{size}" to enqueue an aircraft with an alphanumeric id, type (emergency, vip, passenger, or cargo), and an integer size (<= 0 for small, > 0 for large)

	"/dequeue-AC" to dequeue an aircraft

	"/list-queue" returns a list of the queue contents 

	"/check-last-update" takes a POST request with a string representing a timestamp (ms since epoch) and returns true if this timestamp occured after the most recent update to the controller's queue in the server (used in the UI).

2) Install yarn and node.js if they are not already installed, then run the following from the client directory:

yarn; yarn start 


NOTE: UI needs to be reloaded on server reboot 


